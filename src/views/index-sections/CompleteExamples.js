import React from "react";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components

function CompleteExamples() {
  return (
    <>
      <div className="section">
        <Container className="text-center">
          <Row className="justify-content-md-center">
            <Col lg="8" md="12">
              <h2 className="title">Участвуйте в обсуждениях</h2>
              <h5>
              Постоянно обновляющийся, максимально точный прогноз клёва позволит выбрать лучшее время для выезда, узнать лучшее время для рыбной ловли. Наш прогноз клёва - по своей сути уникальная площадка, без стеснения скажем не имеющая аналогов в мире по совокупности измеряемых данных, а именно: по фазам луны, по направлению ветра, удалённости луны от земли, высоты луны над горизонтом, магнитному полю, атмосферному давлению, температуре воды!
              </h5>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default CompleteExamples;
