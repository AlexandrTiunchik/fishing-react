import React from "react";

// reactstrap components
// import {
// } from "reactstrap";

// core components
import DarkFooter from "../components/Footers/DarkFooter.js";

// sections for this page
import Carousel from "../components/UI/Carousel.js";
import NucleoIcons from "../components/UI/NucleoIcons.js";
import CompleteExamples from "./index-sections/CompleteExamples.js";
import SignUp from "../components/UI/SignUp.js";

function Index() {
  React.useEffect(() => {
    document.body.classList.add("index-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("index-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  return (
    <>
      <div className="wrapper">
        <div className="main">
          <Carousel />
          <NucleoIcons />
          <CompleteExamples />
          <SignUp />
        </div>
        <DarkFooter />
      </div>
    </>
  );
}

export default Index;
