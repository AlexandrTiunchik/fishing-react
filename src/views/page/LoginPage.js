import React, {useContext} from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col
} from "reactstrap";

// core components
import DarkFooter from "../../components/Footers/DarkFooter.js";
import AuthService from "../../services/auth";
import { Link, withRouter } from "react-router-dom";
import {StateContext} from "../../index";

function LoginPage(props) {
  const [loginName, setLoginName] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [error, setError] = React.useState(false);
  const [firstFocus, setFirstFocus] = React.useState(false);
  const [lastFocus, setLastFocus] = React.useState(false);
  React.useEffect(() => {
    document.body.classList.add("login-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("login-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const login = async () => {
    try {
      const user = await new AuthService().login({loginName, password});
      console.log(user);
      dispatchState({type: 'SET_USER', payload: user});
      props.history.push('/');
    } catch (error) {
      setError(true);
      console.error(error.response)
    }
  };

  const [userState, dispatchState] = useContext(StateContext);
  return (
    <>
      <div className="page-header clear-filter" filter-color="blue">
        <div
          className="page-header-image"
          style={{
            backgroundImage: "url(" + require("assets/img/login.jpg") + ")"
          }}
        />
        <div className="content">
          <Container>
            <Col className="ml-auto mr-auto" md="4">
              <Card className="card-login card-plain">
                <Form action="" className="form" method="">
                  <CardHeader className="text-center">
                    <h2>ВХОД</h2>
                    {error && <h4 style={{color: 'red'}}>Ошибка, повторите попытку</h4>}
                  </CardHeader>
                  <CardBody>
                    <InputGroup
                      className={
                        "no-border input-lg" +
                        (firstFocus ? " input-group-focus" : "")
                      }
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="now-ui-icons users_circle-08"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Никнейм..."

                        type="text"
                        onChange={(e) => setLoginName(e.target.value)}
                        onFocus={() => setFirstFocus(true)}
                        onBlur={() => setFirstFocus(false)}
                      />
                    </InputGroup>
                    <InputGroup
                      className={
                        "no-border input-lg" +
                        (lastFocus ? " input-group-focus" : "")
                      }
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="now-ui-icons text_caps-small"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Пароль..."
                        type="password"
                        onChange={(e) => setPassword(e.target.value)}
                        onFocus={() => setLastFocus(true)}
                        onBlur={() => setLastFocus(false)}
                      />
                    </InputGroup>
                  </CardBody>
                  <CardFooter className="text-center">
                    <Button
                      block
                      className="btn-round"
                      color="info"
                      href="#pablo"
                      onClick={login}
                      size="lg"
                    >
                      Войти
                    </Button>
                    <div className="pull-left">
                      <h6>
                        <Link to='registration'>
                          Регистрация
                        </Link>
                      </h6>
                    </div>
                  </CardFooter>
                </Form>
              </Card>
            </Col>
          </Container>
        </div>
        <DarkFooter />
      </div>
    </>
  );
}

export default withRouter(LoginPage);
