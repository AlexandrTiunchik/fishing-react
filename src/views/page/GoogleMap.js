import React, {Component} from 'react';
import { Map, GoogleApiWrapper, Marker, InfoWindow} from 'google-maps-react';


class GoogleMap extends Component {
    state = {
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: {},
    };

    componentWillMount() {

    }

    onMarkerClick = (props, marker, e) =>
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });

    render() {
        return (
            <div>
                <Map
                    className='map'
                    google={this.props.google}
                    initialCenter={this.props.initialCenter}
                    zoom={this.props.zoom}
                    onClick={(event) => console.log(this.state.selectedPlace)}
                >
                    {this.props.marker.map((vul, key) => {
                        const marker = {
                            lat: vul.lat,
                            lng: vul.lng
                        };

                        return (<Marker
                            icon={{
                                url: 'popl.png',
                                anchor: new this.props.google.maps.Point(32,32),
                                scaledSize: new this.props.google.maps.Size(64,64)
                            }}
                            key={key}
                            name={vul.title}
                            text={vul.text}
                            onClick={this.onMarkerClick}
                            position={marker}
                        />)
                    })}

                    <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}>
                        <div>
                            {console.log(this.state.selectedPlace)}
                            <h1>{this.state.selectedPlace.name}</h1>
                            <span>{this.state.selectedPlace.text}</span>
                        </div>
                    </InfoWindow>
                </Map>
            </div>
        );
    };
}

const LoadingContainer = () => (
    <div>Fancy loading container</div>
);

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAT7eiqhZp2fC-8dcB3qEkHsL97IUXk0R4',
    LoadingContainer: LoadingContainer,
})(GoogleMap);
