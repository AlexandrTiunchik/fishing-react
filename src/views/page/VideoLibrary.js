import React, { useState } from "react";

// reactstrap components
import {
  Container,
  Row,
  Col,
  Card,
} from "reactstrap";

// core components
import LandingPageHeader from "../../components/Headers/LandingPageHeader.js";
import DarkFooter from "../../components/Footers/DarkFooter.js";
import useAsyncEffect from "use-async-effect";
import Api from "../../services/api";
import {API} from "../../constants/apiConstants";
import {NEWS_CATEGORY} from "../../constants/newsCategory";
import {Link} from "react-router-dom";

function VideoLibrary() {
  React.useEffect(() => {
    document.body.classList.add("landing-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("landing-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const [newsState, setNews] = useState({
    news: [],
    firstNews: {}
  });

  const getIfreame = (item) => { 
    const frameSrc = item.titleImage.split('/');

    return (
      <Card style={{ width: "20rem", margin: "20px 20px" }} >
        <Link to={`news/${item._id}`}>
          <h3 className="text-center">{item.title}</h3>
          <iframe
            width="100%"
            height="250"
            src={`https://www.youtube.com/embed/${frameSrc[3]}`}
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          >
          </iframe>
        </Link>
      </Card>
    )
  };

  useAsyncEffect(async () => {
    const news = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.VIDEOS}`);

    setNews({
      ...newsState,
      news: news.data,
      firstNews: news.data[0]
    });

  }, []);



  return (
    <>
      <div className="wrapper">
        <LandingPageHeader title="Видео о рыбалке" backgroundImage='https://i.pinimg.com/originals/7e/b6/2d/7eb62d4e057f8932c7491a175e950414.jpg'/>
        <div className="section section-about-us">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto text-left" md="8">
                <h2 className="title">Новые публикации</h2>
              </Col>
            </Row>
          </Container>
        </div>
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" md="12">
              {newsState && newsState.news.map(item => getIfreame(item))}
            </Col>
          </Row>
        </Container>
        <DarkFooter />
      </div>
    </>
  );
}

export default VideoLibrary;