import React, { useState, useEffect } from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Col
} from "reactstrap";

// core components
import { withRouter } from "react-router";
import Find from "../../services/find";

const image = [
  'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSExITFhUVFRUVFRgVFRcXFhUVFRUWFhUVFRUYHSggGBolGxUWITEhJSkrLi4uFx8zODMtNygtLi0BCgoKDg0OFxAQGi0dHR0tKy0tLS0tLSsrLSstKy0xKy0tLS0tLS0rLS0rLS0tLSstLS0tKy0tLS0rNy0tLS0rL//AABEIAKgBKwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAAECBAUGB//EAEAQAAEDAgQDBQUGAwcFAQAAAAEAAhEDIQQSMUEFUWETInGBkTJCobHwBlJicsHRFCMzB0NTgqLh8RVjc5KTJP/EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAoEQEBAAICAQMBCQEAAAAAAAAAAQIRAyESBDFRcQUUIjJBYYGR8BP/2gAMAwEAAhEDEQA/ANKk5WgqIqI9M22WlQsNlGBVYTzCKw9QpMdp5lSJ5IGUc1JrgEjEsVLuhDzLP41xWnhqTqryBs0H3nbAD6tKZNTteSmHleQ1vtpiHggvJOxb/LHoLx5oLuNOMOBe42mDJB3iTPmls9PZgSpArz/7Mfbtjnto13EFxytc8R3tAHHrzXeSgDAqTShNKfMgCkqPaAKJKayZUXt/BN2hQw8ck+dAHbUCnmVcOUg8paEojohQDITgqMmd0lJPjqo0ndAVF3n5pAwgJv8AREZf/lADgVIjkEAcEN3SdiOirqbKo3S0B2152TuG+aFSOIDv6Yn8RszyPveVuqk2juXZj8B4N+ig1hridCT1OnlzU45mfrkq76h5oLqzigLNR4CAXpmtO4TVCEyCqkoYcnKi56ojveoSmceaWcc0Ex2BWGu8FQY481cpwnRBCOqnTahh0KVKqSUgOTCl4IbqmygH9UAUNK8r/tMxbnYvsye7TY2BtmeMzj4wWjyXqGdeXf2mUsuLY8juvpN8y0uBHpl9Uqccl2xSZiCE+KdTPsDYazM720AVdSa5UqZh816F9nOOVWU2PD5EQ5pMtMGDbY21C8zY6F6V/Z1wkvL3PzOpMLDTJBa17yO9E6tBA8beCDektrBSJOyEpdpCtJ4KY21Tdso5pTiaKCphyCCnzIoiwSna5BYUSUjFlPKAHKSk0y2UMjZPKZyDSa13RTzQs/EcTaw5YLqhFmMu4jmdmj8TiB1QRhnVP6rsrf8ADpk/66li7wEDnmQFp+OBJbTGdwsY9hp5PfoD0EnomGGLv6hzfhFmDy97xPoE9MBgAaAGgQABAA6DZTFZAEydU8jmg5iVEsCAstrBT/ihyVJzgFB1VGhtcqYsnZBJKG0J8yAlKG88gpOJQi8pkcu5hRlqG+p1Q+0TJisCuUaagwAaIwRaJEnAJmCE0pSkYxqiUkHOAl2iLZPdWHHnn+WW/QaVyP8AaXgg/DMfHeZUt4OBDh6hvoumq4prRJcAPX4Bc7x/jtF9CqwBzjBbBbZpOjidBfTwU3PH5az0vNe/G6/p5UW9Ck1pVtzTy9EWlQlR5q+75I4TCXDiJuDl2IGxXt/BW5KFJvJo/deZ8E4TUrEBjbTDne60cyefTVepUrAAaAQPAI47bbaOeY4SYz3HLynAQhWhSFZbOVIKQKHmSVJ0sNKkFWD0Zj0jFAUgVBpTpGKE4cg1cQ1jS97mta0S5ziAAOZJsFkjiNWuf/zty0969VpgjfsaRgv/ADOhv5kjamNx1OkAajomzRBLnHkxou49AFUHbVtZoU+VjWcOpu2kPCXX90qeB4YymS6XPqEQ6rUOaoek6Nb+FoA6KyWpGFQwbKQhggEydSXHm5xu49TdSD0YNnZMaMIAZfKUohaFECP+EBINndBcSDBUi+BfRTp1mR18UBXICj4K7Lfw36JPpN2CNjQLQeaY+KkUxhGwgXEJCr0T5wp5gjY0E9jShfw/VWSAUA4fqiUtMKm9HY9UqRRxKoLJemKC0ReU3adUgo8T4mGOyjYXPU7LBxn2gf7hE8iJBWXxPGZnvM+05x8ibfCFRc8QuHP8WW6+u9PlODhmGPXXf1WsZxbtbOLmEaCTl8jqFUw+MfSLrvh2UyMrgcpJEzqOk33QHhrrGyH32dR8FWPXs5ubK8k/H3+49bF03BxNNpeZuGmnBgAEBhy7Zoi5JQsNWaD3mujnP6WUmVmHXunwkK/h2gCSGkHeZHodEZZfJcHB3+HKOw+yOOoimWB897MZEG4AuJ6arpqddp0IK84w+Jawy0AeGi2MLxrpCWPPlj035fsjg5b5eXjlfj2/38uwF1KeQWHguLCdfitplSRINl08XLM3h+u+z+T0tm+8b+qQndEY9CLwoiqto85bAUgq7K3NU+I8ap0SGHM+q72aTBmqO8th1MBI2q0qhW41LjToN7Z4MOIMUqf56mhP4WyfBUKWGr174h3Z0z/cUzqD/jVRc+DYHUrZw9FlNoaxrWtaIAaAAB0AQFalwkPcKmIeazgZa0iKNM29inoTb2nSfBa0oAqqVNyWj2MFPJKGpNKRiCyRxI6KD7hDbQPMJGd191OjU2KRwxQatBwFhPggDVKG4Plsq9TCjlHyUmYnYhPVfIhs9fDl9cigA0sJvtt+6Npug0qjualUugHNQc0F1cbBR01UnOB2TI7CigW0Q6TURrSN0jNfdPZOUMoDlqZhFa4qsyoEUVQFaRwOqqcYxHZ0aj9w0geJsPmifxPJc99scQ7smt+8+PQafH4KcrqVrwYzLkxlcxiX94qGc7ifCyckFxvBnfRSewt28xcFcj6Hu9ohrXWkg8ioBjm6GRyUXOB6HZSLwdbHnsfPZBbl/a/0gXNdqMp+CnSlpibeoKHWYYnUc/8AcIPabJ6ZXPxvcaBqwkMYVSD53UTYpeLT7xfeNOjj3A6rufszxIVGlsgkCf8AkLzVtTRa/Ba+So2pmLcoc62phpOXrJAsiTxylPPlvNw58d73HqdRpABLSAdJGvgq2LxjKTDUqvaxo3dv0A1J8F0TcaWYUVDPshzp8L2Oy5Fho4mt/EZGl7AGg7DUghugN9QJXRhzeV8dPA5PT+OPlsEYnEYn+kDh6R/vHj+c8fgYfYHU36FafDOG06AORt3Xc9xzPeebnm5+SsNapFy205xg5SF0Bp6ozL6JkQJVmkeigxkKcqbVSDNcphAbdWGMUVWkmMkq5TygKtfkkCUqqNAVGpjRaVUbSJ2UqTHtKnSksRgovMzYTuVnYnDvZoQVpsql5n3WyB1doSPC48ynxOGzjdEosYlPEA2dHopuIVt3DctyZn68kM0xpAV7idVUJ+imfG0Iz6Q5eiCaXIoImOvqjFVY5qT6sCyAIS5Nk6hVe0nUqfd5oJxvbFSAJSbUA11UHVirJYps6oXFabTRfMGGyJ5tuD4oYeUPFn+W/wDK75JX2q+P8+P1cW5s6QfO/wAUPO5mhc35ItQNPQ9f3Vd7iNJ8CuSPdz67TOLJs9rXdRY+oTZQ72T5Gx/YoYLT+E/BRqUzuPPYp6Rc8r79/wC/3uk9jmHcfJQL529P2TtcRoT4H6uoz09E2VvwmAmcnaRz9Qul+xfDm1Kjnug9mAQNiXZhfyCcm7oZ3xwuTlWlbv2cqtzgOAIBBvcWvfogcf4KaFUtbdhuw/h5eI0QuGyx0qc4r0vPjhlMr7PeeMUe1wpa0+2wx/mbaYXK8JwAo0wyZOrjzcdT8Pgg4D7Qu7FrSfD9QtDMnwXuuH1X6fFHDk2bohNepg3XU4hWBWGPhVwVJKmt9pyUmAqqxXKT4S2elihSVgOAVQ1kMPJUq3peNVWMKwu2VbB4Rztl0uAwGUCQpqoqswpA0VHimYANHtPOURt953k0E+nNdM7KAsrhlMVR27taglg+5SN2DoSIcepi4AUmyu0DABEAAARoANAreHxYO61K3B8wtCxsXwhzLgeiY2s4hjXiJWNiMOW22SNUg3n9UqmIPinOivapUkKnWqLSxOix3jvQqialUOik0wnERCgRGiZJObKH2R6IgckgOIajsYgyApF6tIzoVXFv7j/yu+RUnOQcY7+W8/hPyKVnSuP88+sclWCquJCsViq5K5I9vk9zZhuE7THsu8k0eaQA+gqZpG+rfNv7JgOs9Rr5hO1o+8PEWUjTPR3UG6FeNv7nbG/kRp5r0D7K4ZtOg0iCXy55HMe6eUBcADrIkb7EeK3Pssa/bZaQLhma2o0GO6+e8ZsIiZKePVZep7w1XU8T4f27MpEESWuOx6nloFwNZpY4tIggwV6ljsSyizI27tzBjwbPibrhsVwrt6hdJ1vET5St8sNvNx5PH3X/ALGYft6oDpyMGafxXy/v5Lsa9MtMHyOxHNZf2Zw9Ogwls5gDMzNjefGFqY2rMdC6Lagk7+nqqx45jijPO5ZItCmUGm5FlIhmlEaq2aE7aqAvMRFVpVFbpFTVHbKvYICRKC2mEanZK1UjquHOaBYBWq+MA0XLMxpFgrFOrIk6DVRs9LePrOeOyaSDUlsj3W++6doGnUhbWEw0AchoOmyp8IwJ/qPEOIsPut2HjuT+wWyE8cfkssiQ6lMFO5yTXhX0hicS4Y102uucq0C0xC7LH4loF9VyfEKmY2KjXbSVRqNnUqhXw24KPVBGqEXKk1Xcz4KLDzRXqrWbFwUyGcoyVXFbmEjUQHIZk+dVRVUmuVoHL0DiNSKRvGYx5C5RgFh8YrS4iIDSAOvdBJ+MeSjky1jXT6TDy5Z+3f8ATLxBVYlTqOQ5WEj0c8t08pw48yoynBTRKKJ3APzTtA+64eCekVaphRa6sOPy/VDDOGdveJaCMwIuWzcDnbZehHiGHwxfSw+XM9xdUeXQS46iblo6QFwwA1gW8lpNrNdYmpT1Add9M/mMlzVrw3drk+0OO4zGtHFY/OYub3LSCB52WjwekHXgxvAg25fBYNKk9jnNsHwCWuu17T7LgdwfvC4tK1eD1GZKj6bSHk/zWFxzS2YIGxkm4135Lq3p5F7dQ80SWkO75BECO83Qh23SfJYnD3ONOXGYe5s8+Z9ZHkqGExZD8znXJBLgIAbrYeE68/NG+z+KH8OWuIsQZJ01zSfNRyZa1PlWGO934abKitU6ipAKTHXSDRbdMaaDSqq0wzogzMBVqk9CTtKQaNGqrAKzaVRaFJ4KmxUogeGgkwABJJ0AGpJV3heMDiHkQ0XYDqfxuG3QbTOpgc6Kvbmf7lp7v/dcPe/8YOn3jfQCbzapSmIuTvsNj2kK4yoCuGwuLI3WnT4iQJVpdJVcOYWVjMVl0Kzq2PcdFSfVLlFVBMXiC7dUzSJVinTlFIIS2pQrULXWXXZC2sVV2WTimyiFVNzkCoVOoguVpCqIclSqIcpk4xqKxFZSB0FvORpsNYRBQYR7RmehteTCPKDxofaLneL1g55vZb2KYA1xDpIHL62XLYo3MLLky3qO/wBHh445Z/wrOKgnKZS0tKU6ZJAGpq1SeBtPhYKkFYokdT4Kco6eHPVXWPe7ZsclqcMdMCbkmRodbnqLrMbmi9hyUsJjXMqtGWxIN+uhB6iU+D86vtGz/h372xv432mbQDlPK4BHgZE+Cy2Ywtq9oLE0yXDr7J+IWjxGpLXD7ubzuAPrqsrFYN7Wdq62cgAHUACb8pN1153T5/Gb6LF43uhg1i53jkr/AA69B9pm3j3Sf0XPjVdBhjlp02/eLnH/AObgP3WG/LLbo1McND4YVWgZaTWiABlqEaaAgNsjfx1VvtNcekBw8iL+qLOUomYHktWCNDi5OwtrqCPEEEhaWG4q3cEeh+V1l1sK1/tNB5SJ8UN2CDbsAYebR3f8zNx4XQHVUscw+8PO3zRs03HwXJ0Kdb/EpeOR3yz/AKo8OYbtLxrma0At5yJkjwukboxVIVbFYs1X/wAODAIDq3PsySAwfmII8AeYWG3iDjOQVyBqS0tAG/8AVifKdFX4Nxa76pqU81V0w+A4MaIZaRBi/mgnf061o0AU88Ln8NxWQDDSOYdb5FXW8UB91w/9T+qYa9PFBW2YmywG4xhOp82uHxIVijiRoHNPgRPolQ36Lwd1eoYbdc/TeStDB4kj3iFOlbbFNkWVPHmNFOpjWxqFlYzGSdUrDlBq1FVc5J9RCL0A1VkqlWEK26oq1esmTPqPUM4T1zJQVROea4t01I5311Hp8EuRv/stCnw+4OwBkkWm+sja3xU6GCdGvONIgC3xn4LnvJi28azOzzSCLGxvaOU+q5evRILhyJAXeVMEQ0xFzz0k78oBlcxxjD5XmPZNwfH6nzV4XHIrlnh7XTCZRnQqzT4dInN/pn5FDqU0SliXt3nxv9eq08YJz5/KX/TP+4B+Zpb8yof9JqzGUeOZsHwurQ4s4QC0XmTfyso1ce0j2L+g+IlLwh/98gDwqoOUcy5oHzQqjg05WuDtpjuz0QalZztbdAIH+/mmo0y6w9eSXjB94zntdNrA0s7Q4uOX3hsI/TVHr0D/ACCQfZptdPSCPhI8lUYAxpvY6/r9eKhXxpFI94kgjKCZykghv+nN6LWSYzqMs+TPO/itrosI0OcXESJsDvE3n60Tced/KAP3hFtbH9FaoxAB1jT9lDjFDNh3ToXCDJnumzgfEwscr5VeM8XM4LDmo9rBq5waPFxgfNegf2gfZ+nhW0auHc4shzH5oOV4pOLXDoYdIvcLmPsnhMlYVSXONPvNFozSALDU33XYfa7iTRgn03RmrPGRs6Ob3nOAOlg71RjNDO7cY6qTtO4Mk26A3lMHEXE68jz0HonptgD1Uq0kWJzSd76TM85KradCU8bUGgB8f+VN+Mq/dA0vYxPSfFVaRI3VhrZbfcjqLA+uvxStGjNfV9rOAJvbUGYIHWDcI1LiNQe6T0PynwQMx23nz/YaqIn9ukI8qNC8R4m91M0y3LmIa4wRDT7YaecStahxSjliCA0ADuyIju/osF1/qylkGU9WlpIuImQY9PRPZaWMfxmgHf0iDbvCabiPzNg+V0mcWoAAntj07at+rhZYddxLTN7b7Rp4KiMzjDbk8rn4eCeyd3S43gYALatP8pc2fEsdfU6qrxDi9Mgsa/PTqMc0dtlaTm3blAlsE953IanXkAWs5PdzN2NPyedPw/mQHVapObO4kk3m/wBbID2fhHHKJp0waskNa0vIMOc0AEyJFyCrtXiDB7zfVeM4TizxvrYk6x4i8K47iz8oyVHhu4JnLtY8vqAlsPTqXFmVM2RwOUw7WQYBgjwISdiVwHAsSWPfD5loMke06eUdfgNlqN4m8awfL63RuHp0/wDEJ/4hc8OJOyh0C9iIdqJOul/3RaPESROSddDygXGwJI157o3BptOqqvVcs5vF27tItJuDFpCJTx7XzlDjAkxB/XVG4BaoQkM4xt7m2tjbx5Jji6f3gnskaNIwW5iQRflbWI5k/Hqp4Wh3oJygxqZBMRldz0sUkl58ddq0yhEkRlkg73gEyeX7LD+0fGcPTPY1GOcC0GGgCIJDe9YjT4dUyS04sd5aLkuo4vEYqnm7mfLtnAkf+pM/VkPtWndJJdjlRc4HceqmGBJJMiFESjZwBA8oTpJG1MHwSvX73ZuYwAEl4IJ5Q03PyU+PYAU2UGNHtVr8y4SCTz1CSSwnJblpt4SY7bTnNE6iTG3jcHqOaFjg51NjZ7msaGScx+fySSVUmjwptOiM+U54y953dgm7upAmwXI8Qxz8RWzunKCWsB2aWmD5pJK4itKgSNASNNJ9UQgCxv8AKTrfpHwSSUXLsyexrBmLgZHoT0VGpiiZjYHr6eiSSneyPRqNIE6jzWjToZxYiZIukklbYeNDqEM1P1Flj4vHwSAd7C1tPrySSV4dlkqmoX3NhsB+3vFNVd7jbNkk27zvzO5a90QLnVJJakFVgbaeF+ttFawDmZ2Nqkinm75AuG7wL/JOkglhuDwri8iu5re0c1gyFxLMsh5ta53jTS9iNwOFaQRiHwbEdm5xIymTJY0ZZgRrB9EklTaPCKGDZULe3e4Zy1gdTLJpxIcSJ3DReJvawJ030MMY/numw9g90B5GsaZANEklNVBKWGwsgGu67bksIDXkEZdLiTM29kzClh6GFzNzV3FpzZoYW5SJyEHLebDTeUkkEFguwFNwqHvkwO6SA0FveBiRqT/k0uApjC4a+Wu/2XEfy3XPuiIEAxcykkg0m0MKMp7Z5v3hkcLZmwJHtHK53prdYtZ/eNybwDe4G6SSA//Z',
  'https://www.lider-press.by/images/articles/2018/June/11/alesy/700_FO84052471_fb484cefb21766749c8735c5480be8d8.jpg',
  'https://i.ytimg.com/vi/M6KEJ1RKFik/maxresdefault.jpg',
  'https://megaribolov.ru/images/siteimage/statiy/raznoe/nezabivaemiy_otdih_na_ribolovnih_bazah/nezabivaemiy_otdih_na_ribolovnih_bazah.jpg',
  'https://истории.рф/wp-content/uploads/2019/05/409240_size2.jpg',
  'https://usadby.by/wp-content/uploads/rybalka-v-belarusi.jpg',
  'https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_5184/https://krasnagorka.by/wp-content/uploads/2018/09/bigstock-192929866.jpg',
  'http://kottedzhi.by/wp-content/uploads/ribalka.jpg',
  'https://cdn24.img.ria.ru/images/149807/51/1498075168_0:98:1500:942_600x0_80_0_0_92c6a987c47c4cd142283d55eeb11474.jpg',
  'https://www.ulovanet.ru/wp-content/uploads/2013/09/rybalka_v_oktyabre_01.jpg',
  'https://cdnimg.rg.ru/img/content/169/09/90/1836_d_850.jpg',
  'https://i.ytimg.com/vi/gMNenkEVLj4/maxresdefault.jpg',
  'https://fishingsblog.ru/wp-content/uploads/2017/07/%D0%BB%D0%B5%D1%82%D0%BD%D1%8F%D1%8F-%D1%80%D1%8B%D0%B1%D0%B0%D0%BB%D0%BA%D0%B0.jpg',
  'https://sannaroch.com/wp-content/uploads/2018/04/rybalka.jpg',
  'https://fishingday.org/wp-content/uploads/2017/03/2-26.jpg',
  'https://sudak.guru/images/529207/kakom_davlenii_luchshe_rybalka.jpg',
  'http://ndn.su/text/img/ribalka-1.jpg',
  'https://www.npp.by/upload/iblock/3a2/3a2752c6bfe3145ac9ccc216a719a2df.JPG',
  'https://sonnikpro.com/wp-content/uploads/2017/03/ryibalka2.jpg',
  'https://i.ytimg.com/vi/7_wo0is3UXE/maxresdefault.jpg',
  'https://fishingday.org/wp-content/uploads/2018/01/2-18.jpg',
  'https://fishingsecrets.info/wp-content/uploads/2018/12/karas-v-oktyabre.jpg',
  'https://liptur.ru/storage/2017/03/15/c8547c04a7f733626df333ccec31b145d108211e_medium.jpg',
  'https://koblevo.org.ua/wp-content/uploads/2017/02/ribalka-koblevo-1200x545_c.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgNbLrtI_Bw4prv26kmjJiSIqhvGFc7iAzbQRgmb2UorZBeNVSyg&s',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQkHrr54MpelTdjIR78DGfn_ZbvSggO6ueH_0hJN24gMhgrlp6&s',
  'https://gtfohota.by/wp-content/uploads/2017/08/20170813_113047.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVKdqXQb2s7LV_mdt4LT4Vk9K77l9ryoIz7Qp9MCPyNa96wWYl&s',
  'http://anton-fisherman.com/wp-content/uploads/2016/05/rybalka_naroch_anton_fisherman2.jpg',
  'https://espanarusa.com/files/autoupload/52/12/2/b0hfnosu417687.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTFPqvmDhqrAQnQQWl5Hj5bswoF7hKz1H1rT2K4SfcDdhD4NMH&s',
  'https://www.visit-belarus.com/wp-content/uploads/2017/07/fisherman-with_spinning_rod-1024x837.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBoHiISX2xXOzO8DYq6j_lMF-_8TrCdiUtX2UjjOXPpgorhuEYug&s',
  'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Ice_fishing_in_miljoonapilkki_fishing_competition.jpg/220px-Ice_fishing_in_miljoonapilkki_fishing_competition.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQChMAVMTDdjh8xG8cdU3gIuJIVWvS6oGXiOK05Yi-wE7XPhbdgUw&s',
  'https://st3.depositphotos.com/12039478/17078/i/450/depositphotos_170788602-stock-photo-fishing-together.jpg',
  'https://siabry.by/images/fish/20150403_122515.jpg',
  'https://megaribolov.ru/images/siteimage/statiy/raznoe/istoriya_razvitiya_ribolovstva/istoriya_razvitiya_ribolovstva.jpg',
  'https://media-cdn.tripadvisor.com/media/photo-s/1a/33/35/0a/caption.jpg',
  'https://fishingday.org/wp-content/uploads/2018/01/1-12-750x350.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTh_rufHEPOietQvXm7N7aV30qvAdinV0alQb_U_GwbkJB0EODlng&s',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyn5Ib16ARVc-nq3Gi9PbgqOyAqJ2TBy_ndfEnlveGz5Uk2AmL&s',
  'http://vboor.by/images/fabout002.jpg',
  'http://crazy-fish.biz/blog/wp-content/uploads/2019/10/IMG-17dd7e126794ffb23297c5cd10355210-V.jpg',
  'https://i.ytimg.com/vi/VY_7Y1d45cQ/maxresdefault.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShuzogE-QPeBs1MI6S0msmAtuM33Dx03D3CaDXt7Amccfa-fig&s'
]


const findItem = (item, index) => {
  const imageID = Math.floor(Math.random() * 10) * Math.floor(Math.random() * 10);
  const srcImage = image[imageID]

  return (
    <div className="row"> 
    {srcImage && item && (
      <>
        <img 
          alt="..."
          src={srcImage}
          style={{
            width: '100px',
            height: '100px',
            marginBottom: '40px'
          }}  
        />
        <div className="col" style={{textAlign: 'left'}}>
          <a
            target='_blank'
            style={{
              display: 'inline-block',
              margin: 5,
              padding: 5,
              color: 'black',
              fontSize: '20pt',
              textAlign: 'left'
            }}
            href={item.link}
          >
            {item.title}
          </a>
          <br />
          <a
            target='_blank'
            style={{
              margin: 5,
              padding: 5,
              color: 'black',
              fontSize: '10pt'
            }}
            href={item.link}
          >
            {item.link.substring(0, 40) + '...'}
          </a>
        </div>
      </>
      )
    } 
    </div>
  )
};

function FindPage(props) {
  const [findResult, setFindResult] = useState(null);
  const [query, setQuery] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const fish = await Find.find('Купить спининг');
      setFindResult(fish.data[0]);
    }
    fetchData();
  }, []);

  const search = async () => {
    const fish = await Find.find(query);
    setFindResult(fish.data[0]);
  };

  return (
    <div className="container mb-4">
      <Row>
        <InputGroup className="no-border input-lg" style={{marginTop: '100px', width: '78%'}}>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="now-ui-icons ui-1_zoom-bold"/>
            </InputGroupText>
          </InputGroupAddon>
          <Input
            placeholder="Поиск"
            type="text"
            onChange={(e) => setQuery(e.target.value)}
            defaultValue='Купить спининг'
          >
          </Input>
        </InputGroup>
        <Button
          className="btn-round"
          color="info"
          href="#pablo"
          onClick={search}
          size="small"
          style={{marginTop: '100px', width: '12%'}}
        >
          Поиск
        </Button>
      </Row>
      <Row>
        <Col md={12}>
          {findResult && findResult.map((item, index) => findItem(item, index))}
        </Col>
      </Row>
    </div>
  );
}

export default withRouter(FindPage);
