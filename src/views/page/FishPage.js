import React, { useState } from "react";

// reactstrap components
import {
  Container,
} from "reactstrap";

// core components
import DarkFooter from "../../components/Footers/DarkFooter.js";
import { withRouter } from "react-router";
import useAsyncEffect from "use-async-effect";
import Api from "../../services/api";
import { API } from "../../constants/apiConstants";

function FishPage(props) {
  React.useEffect(() => {
    document.body.classList.add("landing-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("landing-page");
      document.body.classList.remove("sidebar-collapse");
    };
  }, []);

  const [fish, setFish] = useState();

  useAsyncEffect(async () => {
    const { id } = props.match.params;
    const fish = await new Api().get(`${API.FISH_API.GET_SINGLE_ARTICLE}/${id}`);
    setFish(fish.data);
  }, []);

  return (
    <>
      {fish
        ? (
          <div className="wrapper">
            <img style={{width: '100%'}} src={fish.titleImage} alt="..."/>
            <div className="section">
              <Container>
                <h3 className="title">{fish.title}</h3>
                <h4 className="text-justify">
                  {fish.text}
                </h4>
              </Container>
            </div>
          <DarkFooter />
        </div>)
        : null}
    </>
  );
}

export default withRouter(FishPage);
