import React, { useState } from "react";

// reactstrap components
import {
  Container,
  Row,
  Col
} from "reactstrap";

// core components
import LandingPageHeader from "../../components/Headers/LandingPageHeader.js";
import DarkFooter from "../../components/Footers/DarkFooter.js";
import PostCard from "../../components/UI/PostCard";
import useAsyncEffect from "use-async-effect";
import Api from "../../services/api";
import {API} from "../../constants/apiConstants";
import * as ROUTES from '../../constants/routeConstants';
import {NEWS_CATEGORY} from "../../constants/newsCategory";


function NewsPage() {
  React.useEffect(() => {
    document.body.classList.add("landing-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("landing-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const [newsState, setNews] = useState({
    news: [],
    firstNews: {}
  });

  useAsyncEffect(async () => {
    const news = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.NEWS}`);

    setNews({
      ...newsState,
      news: news.data,
      firstNews: news.data[0]
    });

  }, []);



  return (
    <>
      <div className="wrapper">
        <LandingPageHeader title="Самое важное о рыбалке" backgroundImage={require("assets/img/bg4.jpg")}/>
        <div className="section section-about-us">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto text-left" md="8">
                <h2 className="title">Новые публикации</h2>
              </Col>
            </Row>
            <div className="section-story-overview">
              <Row>
                <Col md="6">
                  <div
                    className="image-container image-left"
                    style={{
                      backgroundImage:
                        "url(" + newsState.firstNews.titleImage + ")"
                    }}
                  >
                  </div>
                </Col>
                <Col md="5">
                  <div
                    className="image-container image-right"
                    style={{
                      backgroundImage:
                        "url(" + newsState.firstNews.titleImage + ")"
                    }}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h3>
                    {newsState.firstNews.title}
                  </h3>
                  <h4>
                    {newsState.firstNews.text && newsState.firstNews.text.substring(0, 500)}...
                  </h4>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <Container>
        <h1>Публикации</h1>
          <Row>
            <Col className="ml-auto mr-auto" md="12">
              {newsState && newsState.news.map(item => (
                <PostCard
                  link={`${ROUTES.NEWS}/${item._id}`}
                  srcImage={item.titleImage}
                  title={item.title}
                  text={item.text}
                  bait={item.bait}
                />
                )
              )}
            </Col>
          </Row>
        </Container>
        <DarkFooter />
      </div>
    </>
  );
}

export default NewsPage;
