import React, { useState, useContext } from "react";

// reactstrap components
import {
  Container,
  Row,
} from "reactstrap";

// core components
import DarkFooter from "../../components/Footers/DarkFooter.js";
import { withRouter } from "react-router";
import useAsyncEffect from "use-async-effect";
import Api from "../../services/api";
import { API } from "../../constants/apiConstants";
import Comment from "../../components/UI/Comment";
import { StateContext } from "../../index";
import ProtectedComponent from "../../components/Protected/ProtectedComponent";
import { ROLE } from "../../constants/roleConstants";
import { NEWS_CATEGORY } from "../../constants/newsCategory"

function PostPage(props) {

  const [userState] = useContext(StateContext);
  const { id } = props.match.params;

  React.useEffect(() => {
    document.body.classList.add("profile-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("profile-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const [post, setPost] = useState();
  const [comment, setComment] = useState();

  const getComment = async () => {
    const comment = await new Api().get(`${API.COMMENT_API.GET_COMMENTS}/${id}`);
    setComment(comment.data);
  };

  useAsyncEffect(async () => {
    const post = await new Api().get(`${API.NEWS_API.GET_SINGLE_ARTICLE}/${id}`);
    setPost(post.data);
    await getComment();
    console.log();
  }, []);

  return (
    <>
      {post
        ? (
          <div className="wrapper">
            {post.category !== NEWS_CATEGORY.VIDEO && <img style={{width: '100%'}} src={post.titleImage}  alt='...' />}
            <div className="section">
              <Container>
                <h1 className="title">{post.title}</h1>
                {post.category === 'VIDEO' && post &&
                  <iframe
                    width="100%"
                    height="550"
                    src={`https://www.youtube.com/embed/${post.titleImage.split('/')[3]}`}
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  >
                  </iframe>
                }
                <h4 className="text-justify ">
                  {post.text}
                </h4>
                <Row>
                  {comment && comment.map(comment =>
                    <>
                      <Comment
                        text={comment.text}
                        srcImage={comment.user.profile.avatar}
                        name={comment.user.profile.name}
                        disable={true}
                      />
                      <div className="form-group col-12">
                        <hr />
                      </div>
                    </>
                  )}
                </Row>
                <Row>
                  <ProtectedComponent role={[ROLE.USER, ROLE.ADMIN]} currentRole={userState.role}>
                    {userState.profile && (
                      <Comment
                        srcImage={userState.profile.avatar}
                        name={userState.profile.name}
                        disable={false}
                        id={id}
                        getComment={getComment}
                      />)
                    }
                  </ProtectedComponent>
                </Row>
              </Container>
            </div>
            <DarkFooter />
          </div>)
        : null}
    </>
  );
}

export default withRouter(PostPage);
