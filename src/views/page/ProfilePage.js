import React, {useState} from "react";

// reactstrap components
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// core components
import ProfilePageHeader from "../../components/Headers/ProfilePageHeader.js";
import DarkFooter from "../../components/Footers/DarkFooter.js";
import useAsyncEffect from "use-async-effect";
import Api from "../../services/api";
import {API} from "../../constants/apiConstants";
import {withRouter} from "react-router";
import Modal from "reactstrap/es/Modal";
import EditProfile from "../../components/Modals/EditProfile";

function ProfilePage(props) {

  const [pills, setPills] = React.useState("1");
  React.useEffect(() => {
    document.body.classList.add("profile-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("profile-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const [profile, setProfile] = React.useState();

  const [modal, setModal] = useState({
    showModal: false,
    modalType: '',
    modalData: null,
  });

  const { id } = props.match.params;

  useAsyncEffect(async () => {
    const profile = await new Api().get(`${API.PROFILE_API.GET}/${id}`);
    setProfile(profile.data);
  }, []);

  const toggle = (modalType, modalData) => () => setModal({
    ...modal,
    showModal: !modal.showModal,
    modalData,
    modalType,
  });

  const updateProfile = (data) => async () => {
    await new Api().put(`${API.PROFILE_API.UPDATE}`, data);
    const profile = await new Api().get(`${API.PROFILE_API.GET}/${id}`);
    toggle()();
    setProfile(profile.data);
  };

  const modalTypes = {
    editProfile: (
      <EditProfile
        imageList={profile ? profile.images : []}
        text="Edit profile"
        data={modal.modalData}
        submit={updateProfile}
        closeModal={toggle()}
      />
    ),
  };

  return (
    <>
      {profile
        ? (<div className="wrapper">
          <ProfilePageHeader
            avatar={profile.avatar}
            name={profile.name}
            surname={profile.surname}
            loginName={profile.user.loginName}
          >
            {profile && profile._id === props.match.params.id
              ? (
                <>
                  <Button
                    style={{zIndex: 100, margin: '30px 0 0 20px'}}
                    className="btn-round btn-icon"
                    color="info"
                    onClick={toggle('editProfile', profile)}
                    id="tooltip515203355"
                  >
                    <i className="fas fa-user-edit" />
                  </Button>
                  <UncontrolledTooltip delay={0} target="tooltip515203355">
                    Изменить
                  </UncontrolledTooltip>
                </>
              )
              : null}
          </ProfilePageHeader>
          <div className="section">
            <Container>
              <h3 className="title">Обо мне</h3>
              <h4 className="description">
                {profile.about}
              </h4>
              <Row>
                <Col className="ml-auto mr-auto" md="6">
                  <h4 className="title text-center">Мои трофеи</h4>
                  <div className="nav-align-center">
                    <Nav
                      className="nav-pills-info nav-pills-just-icons"
                      pills
                      role="tablist"
                    >
                      <NavItem>
                        <NavLink
                          className={pills === "1" ? "active" : ""}
                          onClick={e => {
                            e.preventDefault();
                            setPills("1");
                          }}
                        >
                          <i className="now-ui-icons design_image" />
                        </NavLink>
                      </NavItem>
                      {/*<NavItem>*/}
                      {/*  <NavLink*/}
                      {/*    className={pills === "2" ? "active" : ""}*/}
                      {/*    onClick={e => {*/}
                      {/*      e.preventDefault();*/}
                      {/*      setPills("2");*/}
                      {/*    }}*/}
                      {/*  >*/}
                      {/*    <i className="now-ui-icons location_world"></i>*/}
                      {/*  </NavLink>*/}
                      {/*</NavItem>*/}
                      {/*<NavItem>*/}
                      {/*  <NavLink*/}
                      {/*    className={pills === "3" ? "active" : ""}*/}
                      {/*    onClick={e => {*/}
                      {/*      e.preventDefault();*/}
                      {/*      setPills("3");*/}
                      {/*    }}*/}
                      {/*  >*/}
                      {/*    <i className="now-ui-icons sport_user-run"></i>*/}
                      {/*  </NavLink>*/}
                      {/*</NavItem>*/}
                    </Nav>
                  </div>
                </Col>
                <TabContent className="gallery" activeTab={"pills" + pills}>
                  <TabPane tabId="pills1">
                    <Col className="ml-auto mr-auto" md="10">
                      <Row className="collections">
                        <Col md="12">
                          {profile.images.map(item => {
                            return (
                              <img className="img-raised" alt={item.alt} src={item} />
                            )
                          })}
                        </Col>
                        {/*<Col md="6">*/}
                        {/*  {trophy.old.map(item => {*/}
                        {/*    return (*/}
                        {/*      <img className="img-raised" alt={item.alt} src={item.src}></img>*/}
                        {/*    )*/}
                        {/*  })}*/}
                        {/*</Col>*/}
                      </Row>
                    </Col>
                  </TabPane>
                  {/*<TabPane tabId="pills2">*/}
                  {/*  <Col className="ml-auto mr-auto" md="10">*/}
                  {/*    <Row className="collections">*/}
                  {/*      <Col md="6">*/}
                  {/*        {trophy.old.map(item => {*/}
                  {/*          return (*/}
                  {/*            <img className="img-raised" alt={item.alt} src={item.src}></img>*/}
                  {/*          )*/}
                  {/*        })}*/}
                  {/*      </Col>*/}
                  {/*      <Col md="6">*/}
                  {/*        {trophy.new.map(item => {*/}
                  {/*          return (*/}
                  {/*            <img className="img-raised" alt={item.alt} src={item.src}></img>*/}
                  {/*          )*/}
                  {/*        })}*/}
                  {/*      </Col>*/}
                  {/*    </Row>*/}
                  {/*  </Col>*/}
                  {/*</TabPane>*/}
                  {/*<TabPane tabId="pills3">*/}
                  {/*  <Col className="ml-auto mr-auto" md="10">*/}
                  {/*    <Row className="collections">*/}
                  {/*      <Col md="6">*/}
                  {/*        {trophy.new.map(item => {*/}
                  {/*          return (*/}
                  {/*            <img className="img-raised" alt={item.alt} src={item.src}></img>*/}
                  {/*          )*/}
                  {/*        })}*/}
                  {/*      </Col>*/}
                  {/*      <Col md="6">*/}
                  {/*        {trophy.old.map(item => {*/}
                  {/*          return (*/}
                  {/*            <img className="img-raised" alt={item.alt} src={item.src}></img>*/}
                  {/*          )*/}
                  {/*        })}*/}
                  {/*      </Col>*/}
                  {/*    </Row>*/}
                  {/*  </Col>*/}
                  {/*</TabPane>*/}
                </TabContent>
              </Row>
            </Container>
          </div>
          <DarkFooter />

          <Modal isOpen={modal.showModal} toggle={toggle()}>
            {modalTypes[modal.modalType]}
          </Modal>
        </div>)
        : null}
    </>
  );
}

export default withRouter(ProfilePage);
