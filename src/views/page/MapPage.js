import React from 'react';
import GoogleMap from "./GoogleMap";
import styles from '../../components/styles.module.css';
import {RIVERS} from "../../constants/rivers";

export default function (props) {

    const initialFocus = {
        lat: 53.665451,
        lng: 23.821653,
    };

    return (
        <div style={{backgroundColor: 'blue'}}>
            <div style={{height: 70}}>JOPA</div>
            <div
                className={styles.map}
            >
                <GoogleMap
                    initialCenter={initialFocus}
                    zoom={10}
                    marker={RIVERS}
                />
            </div>
        </div>
    )
}
