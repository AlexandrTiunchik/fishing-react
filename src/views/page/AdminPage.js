import React, {useState} from 'react';
import useAsyncEffect from "use-async-effect";
import Modal from "reactstrap/es/Modal";
import Toast from "reactstrap/es/Toast";
import ToastHeader from "reactstrap/es/ToastHeader";
import ToastBody from "reactstrap/es/ToastBody";
import ModalHeader from "reactstrap/es/ModalHeader";

import Api from "../../services/api";
import AdminTabs from "../../components/UI/AdminTabs";
import { API } from '../../constants/apiConstants';
import { NEWS_CATEGORY } from '../../constants/newsCategory';
import RedactNews from "../../components/Modals/RedactNews";
import RedactFish from "../../components/Modals/RedactFish";
import Delete from "../../components/Modals/Delete";

export default function AdminPage(props) {
  const [state, setState] = useState({
    fishList: [],
    newsList: [],
    discussionList: [],
    videoList: []
  });

  const [modal, setModal] = useState({
    showModal: false,
    modalType: '',
    modalData: null,
  });

  const [popUp, setPopUp] = useState(false);

  useAsyncEffect(async () => {
    const news = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.NEWS}`);
    const discussion = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.DISCUSSION}`);
    const video = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.VIDEOS}`);
    const fish = await new Api().get(API.FISH_API.GET_ALL_ARTICLES);
    setState({
      ...state,
      fishList: fish.data,
      newsList: news.data,
      discussionList: discussion.data,
      videoList: video.data
    });
  }, []);

  const getNews = async () => {
    const news = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.NEWS}`);
    setState({
      ...state,
      newsList: news.data,
    });
  };

  const getFish = async () => {
    const fish = await new Api().get(API.FISH_API.GET_ALL_ARTICLES);
    setState({
      ...state,
      fishList: fish.data,
    });
  };
  

  const getDiscussion = async () => {
    const discussion = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.DISCUSSION}`);
    setState({
      ...state,
      discussionList: discussion.data
    });
  };

  const getVideo = async () => {
    const video = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.VIDEOS}`);
    setState({
      ...state,
      videoList: video.data
    });
  };

  const toggle = (modalType, modalData) => () => setModal({
    ...modal,
    showModal: !modal.showModal,
    modalData,
    modalType,
  });

  const createNews = newsData => async () => {
    await new Api().post(`${API.NEWS_API.CREATE}/?category=${NEWS_CATEGORY.NEWS}`, newsData);
    await getNews();
  };

  const createFish = fishData => async () => {
    await new Api().post(API.FISH_API.CREATE, fishData);
    await getFish()
  };

  const createDiscussion = discussionData => async () => {
    await new Api().post(`${API.NEWS_API.CREATE}/?category=${NEWS_CATEGORY.DISCUSSION}`, discussionData);
    await getDiscussion();
  };

  const createVideo = videoData => async () => {
    await new Api().post(`${API.NEWS_API.CREATE}/?category=${NEWS_CATEGORY.VIDEOS}`, videoData);
    await getVideo();
  };

  const updateNews = (newsData, id) => async () => {
    await new Api().put(`${API.NEWS_API.UPDATE}/?category=${NEWS_CATEGORY.NEWS}`, newsData);
    await getNews();
    toggle()();
  };

  const updateDiscussion = (discussionData, id) => async () => {
    await new Api().put(`${API.NEWS_API.UPDATE}/${id}/?category=${NEWS_CATEGORY.DISCUSSION}`, discussionData);
    await getDiscussion();
    toggle()();
  };

  const updateVideo = (videoData, id) => async () => {
    await new Api().put(`${API.NEWS_API.UPDATE}/${id}/?category=${NEWS_CATEGORY.VIDEOS}`, videoData);
    await getVideo();
    toggle()();
  };

  const updateFish = (fishData, id) => async () => {
    await new Api().put(`${API.FISH_API.UPDATE}/${id}`, fishData);
    await getFish();
    toggle()();
  };

  const deleteNews = id => async () => {
    await new Api().delete(`${API.NEWS_API.DELETE}/${id}/?category=${NEWS_CATEGORY.NEWS}`);
    toggle()();
    await getNews();
  };

  const deleteVideo = id => async () => {
    await new Api().delete(`${API.NEWS_API.DELETE}/${id}/?category=${NEWS_CATEGORY.VIDEOS}`);
    toggle()();
    await getVideo();
  };

  const deleteDiscussion = id => async () => {
    console.log(id);
    await new Api().delete(`${API.NEWS_API.DELETE}/${id}/?category=${NEWS_CATEGORY.DISCUSSION}`);
    toggle()();
    await getDiscussion();
  };

  const deleteFish = id => async () => {
    await new Api().delete(`${API.FISH_API.DELETE}/${id}`);
    toggle()();
    await getFish();
  };

  const modalTypes = {
    redactNews: (
      <RedactNews
        data={modal.modalData}
        closeModal={toggle}
        submit={updateNews}
      />
    ),
    redactFish: (
      <RedactFish
        data={modal.modalData}
        closeModal={toggle}
        submit={updateFish}
      />
    ),
    redactDiscussion: (
      <RedactFish
        data={modal.modalData}
        closeModal={toggle}
        submit={updateDiscussion()}
      />
    ),
    redactVideo: (
      <RedactFish
        data={modal.modalData}
        closeModal={toggle}
        submit={updateVideo()}
      />
    ),
    deleteNews: (
      <Delete
        title="Удаление"
        text="Вы действительно хотите удалить новость?"
        data={modal.modalData}
        delete={deleteNews}
        closeModal={toggle()}
      />
    ),
    deleteFish: (
      <Delete
        title="Удаление"
        text="Вы действительно хотите удалить информацию о рыбе?"
        data={modal.modalData}
        delete={deleteFish}
        closeModal={toggle()}
      />
    ),
    deleteDiscussion: (
      <Delete
        title="Удаление"
        text="Вы действительно хотите удалить обсуждение?"
        data={modal.modalData}
        delete={deleteDiscussion}
        closeModal={toggle()}
      />
    ),
    deleteVideo: (
      <Delete
        title="Удаление"
        text="Вы действительно хотите удалить видео?"
        data={modal.modalData}
        delete={deleteVideo}
        closeModal={toggle()}
      />
    )
  };

  return (
    <div>
      <AdminTabs
        newsList={state.newsList}
        discussionList={state.discussionList}
        fishList={state.fishList}
        videoList={state.videoList}
        createNews={createNews}
        createFish={createFish}
        createDiscussion={createDiscussion}
        createVideo={createVideo}
        showModal={toggle}
      />

      <Modal isOpen={modal.showModal} toggle={toggle()}>
        <ModalHeader toggle={toggle()}>Внимание</ModalHeader>
        {modalTypes[modal.modalType]}
      </Modal>

      <Toast isOpen={popUp} style={{position: 'fixed', top: 0, right: 0}}>
        <ToastHeader>
          Отлично!
        </ToastHeader>
        <ToastBody>
          Операция прошла успешно
        </ToastBody>
      </Toast>
    </div>
  )
};
