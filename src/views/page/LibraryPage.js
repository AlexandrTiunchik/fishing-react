import React, { useState } from "react";

// reactstrap components
import {
  Container,
  Row,
  Col
} from "reactstrap";

// core components
import LandingPageHeader from "../../components/Headers/LandingPageHeader.js";
import DarkFooter from "../../components/Footers/DarkFooter.js";
import PostCard from "../../components/UI/PostCard";
import useAsyncEffect from "use-async-effect";
import Api from "../../services/api";
import { API } from "../../constants/apiConstants";

function LibraryPage() {
  React.useEffect(() => {
    document.body.classList.add("landing-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("landing-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const [fishState, setFish] = useState({
    fish: [],
  });

  useAsyncEffect(async () => {
    const fish = await new Api().get(API.FISH_API.GET_ALL_ARTICLES);
    setFish({
      fish: fish.data,
      posterFish: fish.data[0].titleImage
    });
  }, []);

  return (
    <>
      <div className="wrapper">
        <LandingPageHeader title="Каталог рыб" backgroundImage='https://i.pinimg.com/originals/7e/b6/2d/7eb62d4e057f8932c7491a175e950414.jpg'/>
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" md="12">
              {fishState && fishState.fish.map(fish => (
                <PostCard
                  link={`fish/${fish._id}`}
                  srcImage={fish.titleImage}
                  title={fish.title}
                />
                )
              )}
            </Col>
          </Row>
        </Container>
        <DarkFooter />
      </div>
    </>
  );
}

export default LibraryPage;
