import React, {useContext} from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col
} from "reactstrap";

// core components
import DarkFooter from "../../components/Footers/DarkFooter.js";
import AuthService from "../../services/auth";
import { withRouter } from "react-router-dom";
import { StateContext } from "../../index";

function RegistrationPage(props) {
  const [ user, setUser ] = React.useState({
    loginName: '',
    password: '',
    avatar: ''
  });
  const [firstFocus, setFirstFocus] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [lastFocus, setLastFocus] = React.useState(false);
  const [userState, dispatchState] = useContext(StateContext);
  React.useEffect(() => {
    document.body.classList.add("login-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("login-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const changeHandler = event => {
    const {value, name} = event.target;
    setUser({
      ...user,
      [name]: value
    });
  };

  const registration = async () => {
    try {
      const response = await new AuthService().registration(user);
      dispatchState({type: 'SET_USER', payload: response});
      props.history.push('/');
    } catch (error) {
      setError(true);
      console.error(error.response);
    }
  };

  return (
    <>
      <div className="page-header clear-filter" filter-color="blue">
        <div
          className="page-header-image"
          style={{
            backgroundImage: "url(" + require("assets/img/login.jpg") + ")"
          }}
        />
        <div className="content">
          <Container>
            <Col className="ml-auto mr-auto" md="4">
              <Card className="card-login card-plain">
                <Form action="" className="form" method="">
                  <CardHeader className="text-center">
                      <h2>РЕГИСТРАЦИЯ</h2>
                      {error && <h4 style={{color: 'red'}}>Ошибка, повторите попытку</h4>}
                  </CardHeader>
                  <CardBody>

                    <InputGroup
                      className={
                        "no-border input-lg" +
                        (firstFocus ? " input-group-focus" : "")
                      }
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="now-ui-icons users_circle-08"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Никнейм"
                        type="text"
                        name="loginName"
                        onChange={changeHandler}
                        onFocus={() => setFirstFocus(true)}
                        onBlur={() => setFirstFocus(false)}
                      />
                    </InputGroup>

                    <InputGroup
                      className={
                        "no-border input-lg" +
                        (lastFocus ? " input-group-focus" : "")
                      }
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="now-ui-icons text_caps-small"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Email"
                        name="email"
                        onChange={changeHandler}
                        type="email"
                        onFocus={() => setLastFocus(true)}
                        onBlur={() => setLastFocus(false)}
                      />
                    </InputGroup>

                    <InputGroup
                        className={
                          "no-border input-lg" +
                          (lastFocus ? " input-group-focus" : "")
                        }
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="now-ui-icons text_caps-small"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Пароль"
                        name="password"
                        type="password"
                        onChange={changeHandler}
                        onFocus={() => setLastFocus(true)}
                        onBlur={() => setLastFocus(false)}
                      />
                    </InputGroup>

                  </CardBody>
                  <CardFooter className="text-center">
                    <Button
                      block
                      className="btn-round"
                      color="info"
                      href="#pablo"
                      onClick={registration}
                      size="lg"
                    >
                      Создать аккаунт
                    </Button>
                  </CardFooter>
                </Form>
              </Card>
            </Col>
          </Container>
        </div>
        <DarkFooter />
      </div>
    </>
  );
}

export default withRouter(RegistrationPage);
