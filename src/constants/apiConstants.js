export const BASE_URL = 'https://fishing-nodejs.herokuapp.com/api';
export const AUTHENTICATION_URL = '/authentication';
export const COMMENT_URL = '/comment';
export const FISH_URL = '/fish';
export const NEWS_URL = '/news';
export const VOTE_URL = '/vote';
export const USER_URL = '/user';
export const PROFILE_URL = '/profile';

export const AUTHENTICATION_API = {
  LOGIN: `${AUTHENTICATION_URL}/login`,
  REGISTRATION: `${AUTHENTICATION_URL}/registration`,
};

export const COMMENT_API = {
  CREATE: `${COMMENT_URL}`, // PLUS NEWS ID
  DELETE: `${COMMENT_URL}`, // PLUS NEWS ID
  GET_COMMENTS: `${COMMENT_URL}`, // PLUS NEWS ID
};

export const FISH_API = {
  GET_ALL_ARTICLES: `${FISH_URL}`,
  GET_SINGLE_ARTICLE: `${FISH_URL}`, // PLUS FISH ID
  UPDATE: `${FISH_URL}`, // PLUS FISH ID
  CREATE: `${FISH_URL}`,
  DELETE: `${FISH_URL}`, // PLUS FISH ID
};

export const NEWS_API = {
  GET_ALL_ARTICLES: `${NEWS_URL}`,
  GET_SINGLE_ARTICLE: `${NEWS_URL}`, // PLUS NEWS ID
  UPDATE: `${NEWS_URL}`, // PLUS NEWS ID
  CREATE: `${NEWS_URL}`,
  DELETE: `${NEWS_URL}`, // PLUS NEWS ID
};

export const VOTE_API = {
  GET_ALL: `${VOTE_URL}`, // PLUS NEWS ID
  CREATE: `${VOTE_URL}`,  // PLUS NEWS ID
  DELETE: `${VOTE_URL}`,  // PLUS COMMENT ID
};

export const USER_API = {
  GET: `${USER_URL}`,
};

export const PROFILE_API = {
  GET: `${PROFILE_URL}`, // PLUS PROFILE ID
  UPDATE: `${PROFILE_URL}`, // PLUS PROFILE ID
};

export const API = {
  AUTHENTICATION_API,
  COMMENT_API,
  FISH_API,
  NEWS_API,
  VOTE_API,
  USER_API,
  PROFILE_API,
};

