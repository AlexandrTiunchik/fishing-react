export const MAIN = '/';
export const FISH_POST = '/fish/:id';
export const NEWS_POST = '/news/:id';
export const ADMIN = '/admin';
export const NEWS = '/news';
export const LIBRARY = '/library';
export const PROFILE = '/profile/:id';
export const LOGIN = '/login';
export const REGISTRATION = '/registration';
export const MAP = '/map';
export const DISCUSSION = '/discussion';
export const FIND = '/find';
export const VIDEO = '/video'
