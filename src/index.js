import React, { useContext, useReducer} from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Auth from './services/auth';
// styles for this kit
import "assets/css/bootstrap.min.css";
import "assets/scss/now-ui-kit.scss";
import "assets/demo/demo.css";
import "assets/demo/nucleo-icons-page-styles.css";
// pages for this kit
import Index from "views/Index.js";
import LoginPage from "views/page/LoginPage.js";
import NewsPage from "views/page/NewsPage.js";
import ProfilePage from "views/page/ProfilePage.js";
import LibraryPage from "views/page/LibraryPage.js";
import VideoLibrary from "views/page/VideoLibrary";
import FishPage from "views/page/FishPage.js";
import RegistrationPage from "./views/page/RegistrationPage";
import AdminPage from "./views/page/AdminPage";
import IndexNavbar from './components/Navbars/IndexNavbar';
import useAsyncEffect from "use-async-effect";
import * as ROUTES from './constants/routeConstants'
import PostPage from "./views/page/PostPage";
import MapPage from "./views/page/MapPage";
import DiscussionPage from "./views/page/DiscussionPage";
import FindPage from "./views/page/FindPage";

export const StateContext = React.createContext();
const initialState = {
  role: 'guest',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_USER': {
      return {
        ...state,
        ...action.payload
      }
    }

    default: return state;
  }
};

const StateProvider = ({ children }) => {
  const contextValue = useReducer(reducer, initialState);
  return (
    <StateContext.Provider value={contextValue}>
      {children}
    </StateContext.Provider>
  );
};

const App = () => {
  const [userState, dispatchState] = useContext(StateContext);

  useAsyncEffect(async () => {
    try {
      const user = await new Auth().initApp();
      dispatchState({type: 'SET_USER', payload: user});
    } catch (e) {
      //await new Auth().logout();
    }
  }, []);

  return (
      <BrowserRouter>
        <IndexNavbar currentRole={userState.role}/>
        <Switch>
          <Switch>
            <Route
              exact
              path={ROUTES.MAIN}
              render={props => <Index {...props} />}
            />
            <Route
              exact
              path={ROUTES.ADMIN}
              render={props => <AdminPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.LIBRARY}
              render={props => <LibraryPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.FISH_POST}
              render={props => <FishPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.NEWS}
              render={props => <NewsPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.DISCUSSION}
              render={props => <DiscussionPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.FIND}
              render={props => <FindPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.NEWS_POST}
              render={props => <PostPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.PROFILE}
              render={props => <ProfilePage {...props} />}
            />
            <Route
              exact
              path={ROUTES.LOGIN}
              render={props => <LoginPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.REGISTRATION}
              render={props => <RegistrationPage {...props} />}
            />
            <Route
              exact
              path={ROUTES.VIDEO}
              render={props => <VideoLibrary {...props} />}
            />
            <Route
              exact
              path={ROUTES.MAP}
              render={props => <MapPage {...props} />}
            />
            <Redirect to={ROUTES.MAIN} />
          </Switch>
        </Switch>
      </BrowserRouter>
  )
};

ReactDOM.render(
  <StateProvider>
    <App />
  </StateProvider>,
  document.getElementById("root")
);
