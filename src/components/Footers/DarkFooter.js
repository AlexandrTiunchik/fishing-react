/*eslint-disable*/
import React from "react";

// reactstrap components
import {
  Container,

} from "reactstrap";

function DarkFooter() {
  return (
    <footer className="footer"  data-background-color="black">
      <Container>
        <nav>
          <a
            href="https://vk.com/id112681686"
            target="_blank"
          >
            О нас
          </a>
        </nav>
        <div className="copyright" id="copyright">
          © {new Date().getFullYear()}
        </div>
      </Container>
    </footer>
  );
}

export default DarkFooter;
