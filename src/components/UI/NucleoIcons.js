import React from "react";
// reactstrap components
import { Button, Container, Row, Col } from "reactstrap";
import * as ROUTES from '../../constants/routeConstants'
// core components

function NucleoIcons() {
  return (
    <>
      <div className="section section-nucleo-icons">
        <Container>
          <Row className="align-items-center">
            <Col lg="6" md="12">
              <h3 className="title">Огромное рыболовное сообщество</h3>
              <h5>
              Рыболовное сообщество приветствует всех, для кого рыбалка — нечто больше, чем просто увлекательное хобби!
              На нашей площадке ежедневно общаются любители рыбной ловли.
              Они ведут свои рыболовные блоги, публикуют фото уловов, хвастаются трофейными экземплярами, обсуждают всё, что связано с рыбалкой,
              делятся опытом, советами, как положено истинному рыбаку, много шутят.
              Присоединяйтесь и Вы! Любители отдохнуть с удочкой, азартные спиннингисты, поплавочники, почитатели донок и фидерной ловли, карпятники, форельщики, щукари и универсальные рыбаки любители — здесь
              интересно всем! Каждый зарегистрированный пользователь может вести свой блог, общаться, получать много интереснейшей и полезной информации, находить новых товарищей по рыбной ловли.
              </h5>
              <Button
                className="btn-round mr-1"
                color="info"
                href={ROUTES.NEWS}
                size="lg"
              >
                К новостям
              </Button>
            </Col>
            <Col lg="6" md="12">
              <div className="icons-container justify-center">
                <i className="now-ui-icons ui-1_send"/>
                <i className="now-ui-icons ui-2_like"/>
                <i className="now-ui-icons transportation_air-baloon"/>
                <i className="now-ui-icons text_bold"/>
                <i className="now-ui-icons tech_headphones"/>
                <i className="now-ui-icons emoticons_satisfied"/>
                <i className="now-ui-icons shopping_cart-simple"/>
                <i className="now-ui-icons objects_spaceship"/>
                <i className="now-ui-icons media-2_note-03"/>
                <i className="now-ui-icons ui-2_favourite-28"/>
                <i className="now-ui-icons design_palette"/>
                <i className="now-ui-icons clothes_tie-bow"/>
                <i className="now-ui-icons location_pin"/>
                <i className="now-ui-icons objects_key-25"/>
                <i className="now-ui-icons travel_istanbul"/>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default NucleoIcons;
