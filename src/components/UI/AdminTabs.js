import React, {useState} from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
} from "reactstrap";
import Input from "reactstrap/es/Input";
import CardImg from "reactstrap/es/CardImg";
import Button from "reactstrap/es/Button";
import { NEWS_CATEGORY } from '../../constants/newsCategory'
// core components

function AdminTabs(props) {
  const [iconPills, setIconPills] = useState("1");
  const [pills, setPills] = useState("1");

  const [newsState, setNews] = useState({
    Title: '',
    TitleImage: '',
    category: NEWS_CATEGORY.NEWS,
    text: '',
  });

  const [discussionState, setDiscussionState] = useState({
    Title: '',
    TitleImage: '',
    category: NEWS_CATEGORY.DISCUSSION,
    text: '',
  });

  const [videoState, setVideo] = useState({
    title: '',
    titleImage: '',
    category: NEWS_CATEGORY.VIDEOS,
    text: '',
  });

  const [fishState, setFish] = useState({
    title: '',
    titleImage: '',
    text: '',
  });


  const getIfreame = (frame) => { 
    const frameSrc = frame.split('/');

    return (
      <iframe
        width="200"
        height="150"
        src={`https://www.youtube.com/embed/${frameSrc[3]}`}
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      >
      </iframe>
    )
  };

  const changeHandler = (state, setState) => event => {
    const {value, name} = event.target;
    setState({
      ...state,
      [name]: value
    });
  };

  return (
    <>
      <div className="section section-tabs">
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" md="10" xl="6">
              <p className="category">Панель добавления информации</p>
              <Card>
                <CardHeader>
                  <Nav className="justify-content-center" role="tablist" tabs>
                    <NavItem>
                      <NavLink
                        className={iconPills === "1" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("1");
                        }}
                      >
                        <i className="now-ui-icons objects_umbrella-13"/>
                        Добавить новость
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "3" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("3");
                        }}
                      >
                        <i className="now-ui-icons objects_umbrella-13"/>
                        Добавить тему для обсуждения
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "2" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("2");
                        }}
                      >
                        <i className="now-ui-icons objects_umbrella-13"/>
                        Добавить рыбу
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "4" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("4");
                        }}
                      >
                        <i className="now-ui-icons objects_umbrella-13"/>
                        Добавить видео
                      </NavLink>
                    </NavItem>
                  </Nav>
                </CardHeader>
                <CardBody>
                  <TabContent
                    className="text-center"
                    activeTab={"iconPills" + iconPills}
                  >
                    <TabPane tabId="iconPills1">

                      <Input
                        name="title"
                        className="mb-2"
                        placeholder="Заголовок"
                        value={newsState.title}
                        onChange={changeHandler(newsState, setNews)}
                      />

                      <Input
                        name="titleImage"
                        className="mb-2"
                        placeholder="Ссылка на картинку"
                        value={newsState.titleImage}
                        onChange={changeHandler(newsState, setNews)}
                      />

                      <Input
                        name="text"
                        className="mb-2"
                        placeholder="Описание"
                        type="textarea"
                        value={newsState.text}
                        onChange={changeHandler(newsState, setNews)}
                      />

                      <Button color="success" onClick={props.createNews(newsState)}>Опубликовать</Button>
                    </TabPane>
                    <TabPane tabId="iconPills2">

                      <Input
                        name="title"
                        className="mb-2"
                        placeholder="Заголовок"
                        value={fishState.title}
                        onChange={changeHandler(fishState, setFish)}
                      />

                      <Input
                        name="titleImage"
                        className="mb-2"
                        placeholder="Ссылка на картинку"
                        value={fishState.titleImage}
                        onChange={changeHandler(fishState, setFish)}
                      />

                      <Input
                        name="text"
                        className="mb-2"
                        placeholder="Описание"
                        type="textarea"
                        value={fishState.text}
                        onChange={changeHandler(fishState, setFish)}
                      />

                      <Button color="success" onClick={props.createFish(fishState)} >Создать</Button>
                    </TabPane>

                    <TabPane tabId="iconPills3">

                      <Input
                        name="title"
                        className="mb-2"
                        placeholder="Заголовок"
                        value={discussionState.title}
                        onChange={changeHandler(discussionState, setDiscussionState)}
                      />

                      <Input
                        name="titleImage"
                        className="mb-2"
                        placeholder="Ссылка на картинку"
                        value={discussionState.titleImage}
                        onChange={changeHandler(discussionState, setDiscussionState)}
                      />

                      <Input
                        name="text"
                        className="mb-2"
                        placeholder="Описание"
                        type="textarea"
                        value={discussionState.text}
                        onChange={changeHandler(discussionState, setDiscussionState)}
                      />

                      <Button color="success" onClick={props.createDiscussion(discussionState)} >Создать</Button>
                    </TabPane>
                    <TabPane tabId="iconPills4">
                      <Input
                        name="title"
                        className="mb-2"
                        placeholder="Заголовок"
                        value={videoState.title}
                        onChange={changeHandler(videoState, setVideo)}
                      />

                      <Input
                        name="titleImage"
                        className="mb-2"
                        placeholder="Ссылка на видео"
                        value={videoState.src}
                        onChange={changeHandler(videoState, setVideo)}
                      />

                      <Input
                        name="text"
                        className="mb-2"
                        placeholder="Описание"
                        type="textarea"
                        value={videoState.text}
                        onChange={changeHandler(videoState, setVideo)}
                      />

                      <Button color="success" onClick={props.createVideo(videoState)} >Создать</Button>
                      </TabPane>
                  </TabContent>
                </CardBody>
              </Card>
            </Col>
            <Col className="ml-auto mr-auto" md="10" xl="6">
              <p className="category">Добавленые записи</p>
              <Card>
                <CardHeader>
                  <Nav
                    className="nav-tabs-neutral justify-content-center"
                    data-background-color="blue"
                    role="tablist"
                    tabs
                  >
                    <NavItem>
                      <NavLink
                        className={pills === "1" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setPills("1");
                        }}
                      >
                        Новости
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={pills === "2" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setPills("2");
                        }}
                      >
                        Обсуждения
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={pills === "3" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setPills("3");
                        }}
                      >
                        Рыбы
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={pills === "4" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setPills("4");
                        }}
                      >
                        Видео
                      </NavLink>
                    </NavItem>
                  </Nav>
                </CardHeader>
                <CardBody>

                  <TabContent
                    className="text-center"
                    activeTab={"pills" + pills}
                    style={{maxHeight: 600, overflowX: 'hidden'}}
                  >
                    <TabPane
                      className="row p-1"
                      tabId="pills1"
                    >

                      {props.newsList.map(news => (
                          <Card
                            key={news._id}
                            className="col-md-5 m-1"
                          >
                            <CardImg width="10%" src={news.titleImage}/>
                            <CardBody className="p-1">
                              <h4 className="m-0">{news.title}</h4>
                            </CardBody>
                            <div>
                              <Button
                                onClick={props.showModal('redactNews', news)}
                                color="info"
                              >
                                Редактировать
                              </Button>

                              <Button
                                color="danger"
                                onClick={props.showModal('deleteNews', news)}
                              >
                                Удалить
                              </Button>
                            </div>
                          </Card>
                        )
                      )}

                    </TabPane>

                    <TabPane
                      className="row p-1"
                      tabId="pills2"
                    >
                      {props.discussionList.map(discuss => (
                          <Card
                            key={discuss._id}
                            className="col-md-5 m-1"
                          >
                            <CardImg width="10%" src={discuss.titleImage}/>
                            <CardBody className="p-1">
                              <h4 className="m-0">{discuss.title}</h4>
                            </CardBody>
                            <div>
                              <Button
                                onClick={props.showModal('redactDiscussion', discuss)}
                                color="info"
                              >
                                Редактировать
                              </Button>

                              <Button
                                color="danger"
                                onClick={props.showModal('deleteDiscussion', discuss)}
                              >
                                Удалить
                              </Button>
                            </div>
                          </Card>
                        )
                      )}
                    </TabPane>

                    <TabPane
                      className="row p-1"
                      tabId="pills3"
                    >
                      {props.fishList.map(fish => (
                          <Card
                            key={fish._id}
                            className="col-md-5 m-1"
                          >
                            <CardImg width="10%" src={fish.titleImage}/>
                            <CardBody className="p-1">
                              <h4 className="m-0">{fish.title}</h4>
                            </CardBody>
                            <div>
                              <Button
                                onClick={props.showModal('redactFish', fish)}
                                color="info"
                              >
                                Редактировать
                              </Button>

                              <Button
                                color="danger"
                                onClick={props.showModal('deleteFish', fish)}
                              >
                                Удалить
                              </Button>
                            </div>
                          </Card>
                        )
                      )}
                    </TabPane>

                    <TabPane
                      className="row p-1"
                      tabId="pills4"
                    >
                      {props.videoList && props.videoList.map(video => (
                          <Card
                            key={video._id}
                            className="col-md-5 m-1"
                          >
                            {getIfreame(video.titleImage)}
                            <CardBody className="p-1">
                              <h4 className="m-0">{video.title}</h4>
                            </CardBody>
                            <div>
                              <Button
                                onClick={props.showModal('redactVideo', video)}
                                color="info"
                              >
                                Редактировать
                              </Button>

                              <Button
                                color="danger"
                                onClick={props.showModal('deleteVideo', video)}
                              >
                                Удалить
                              </Button>
                            </div>
                          </Card>
                        )
                      )}
                    </TabPane>

                  </TabContent>

                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default AdminTabs;
