import React, { useState } from "react";

import {
  Row,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import Api from "../../services/api";
import { API } from "../../constants/apiConstants";
import Button from "reactstrap/es/Button";


function BaitChoice(props) {

  const { baits, id } = props;

  const [currentBait, setCurrentBaits] = React.useState();

  const sendVote = async () => {
    await  new Api().post(`${API.VOTE_API}/${id}`);
  };

  return (
    <FormGroup check className="form-check-radio blockquote blockquote-info" style={{width: '400px'}}>
      {baits.map(bait =>
        <Row>
          <Label check>
            <Input
              type="radio"
              name="baits"
              onChange={() => setCurrentBaits(bait)}
            />
            <span className="form-check-sign"/>
            {bait}
          </Label>
        </Row>
      )}
      <Button
        block
        className="btn-morphing"
        color="info"
        size="lg"
        onClick={sendVote}
      >
        Проголосовать
      </Button>
    </FormGroup>
  )
}

export default BaitChoice;
