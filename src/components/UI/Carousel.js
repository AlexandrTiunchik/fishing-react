import React, { useState } from "react";

// reactstrap components
import {
  Container,
  Row,
  Col,
  Carousel,
  CarouselItem,
  CarouselIndicators,
  CardImg,
  Card,
  CardTitle,
  CardBody,
} from "reactstrap";

// core components
import useAsyncEffect from "use-async-effect";
import Api from '../../services/api';
import { API } from '../../constants/apiConstants';
import { NEWS_CATEGORY } from '../../constants/newsCategory';
import  * as ROUTES  from '../../constants/routeConstants';
import { Link } from "react-router-dom";

const items = [
  {
    src: require("assets/img/ryba1.jpg"),
    altText: "Nature, United States",
    caption: "Рыбалка на Нарочи"
  },
  {
    src: require("assets/img/bg3.jpg"),
    altText: "Somewhere Beyond, United States",
    caption: "Поиски окуня в Беларуси"
  },
  {
    src: require("assets/img/bg4.jpg"),
    altText: "Yellowstone National Park, United States",
    caption: "Спорт или отдых душой?"
  }
];

function CarouselSection() {

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };

  const onExited = () => {
    setAnimating(false);
  };

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const [newsState, setNews] = useState({
    news: [],
    discussion: []
  });

  useAsyncEffect(async () => {
    const news = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.NEWS}`);
    const discussion = await new Api().get(`${API.NEWS_API.GET_ALL_ARTICLES}?category=${NEWS_CATEGORY.DISCUSSION}`);

    console.log(discussion.data);
    setNews({
      news: news.data.slice(0,4),
      discussion: discussion.data.slice(0,4)
    });

  }, []);

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = newIndex => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <div className="section" id="carousel">
      <Container style={{
        paddingTop: "50px",
        width: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
      }}>
        <h1 >Рыбалка в Беларуси</h1>
        <Row>
          <Col lg="2" md="2">
            <h3>Новости</h3>
            {newsState.news.map(item => (
              <>
                <Card style={{ margin: "20px 10px 20px 0" }} >
                  <CardImg
                    alt="..."
                    src={item.titleImage}
                    top
                  />
                  <CardBody>
                    <CardTitle style={{ fontSize: '13px'}} tag="span">{item.title.substring(0,10)}...</CardTitle>
                    <br />
                    <Link to={`${ROUTES.NEWS}/${item._id}`}>Подробнее</Link>
                  </CardBody>
                </Card>
              </>
            ))}
          </Col>
          <Col lg="8" md="8" className="d-flex align-items-center">
            <Carousel
              activeIndex={activeIndex}
              next={next}
              previous={previous}
            >
              <CarouselIndicators
                items={items}
                activeIndex={activeIndex}
                onClickHandler={goToIndex}
              />
              {items.map(item => {
                return (
                  <CarouselItem
                    onExiting={onExiting}
                    onExited={onExited}
                    key={item.src}
                  >
                    <img src={item.src} alt={item.altText} />
                    <div className="carousel-caption d-none d-md-block">
                      <h3>{item.caption}</h3>
                    </div>
                  </CarouselItem>
                );
              })}
              <a
                className="carousel-control-prev"
                data-slide="prev"
                href="#pablo"
                onClick={e => {
                  e.preventDefault();
                  previous();
                }}
                role="button"
              >
                <i className="now-ui-icons arrows-1_minimal-left"/>
              </a>
              <a
                className="carousel-control-next"
                data-slide="next"
                onClick={e => {
                  e.preventDefault();
                  next();
                }}
                role="button"
              >
                <i className="now-ui-icons arrows-1_minimal-right"/>
              </a>
            </Carousel>
          </Col>
          <Col lg="2" md="2">
            <h3 style={{textAlign: "right"}} >Обсуждения</h3>
            {newsState.discussion.map(item => (
              <>
                <Card style={{ margin: "20px 10px 20px 0" }} >
                  <CardImg
                    alt="..."
                    src={item.titleImage}
                    top
                  />
                  <CardBody>
                    <CardTitle style={{ fontSize: '13px'}} tag="span">{item.title.substring(0,10)}...</CardTitle>
                    <br />
                    <Link to={`${ROUTES.NEWS}/${item._id}`}>Подробнее</Link>
                  </CardBody>
                </Card>
              </>
            ))}
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default CarouselSection;
