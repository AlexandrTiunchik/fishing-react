import React, { useState } from "react";

// reactstrap components
import {
  Container,
  Col,
  Row,
  Input,
  Button,
} from "reactstrap";
import Api from "../../services/api";
import { API } from "../../constants/apiConstants";


function Comment(props) {
  const { srcImage, text, name, disable, id, getComment } = props;
  const [ comment, setComment ] = useState(text);
  // TODO: мб создать конастанут с состояниями и сэтить ее ? (-_-)
  const [ state, setState ] = useState(true);

  const sendComment = async () => {
    await new Api().post(`${API.COMMENT_API.CREATE}/${id}`, { text: comment });
    getComment();
    setState(!state.state);
  };

  const activeComment = () => {
    return (
      <>
        <Input
          type='textarea'
          value={comment}
          placeholder='Оставте коментарий'
          disabled={disable}
          onChange={(e) => setComment(e.target.value)}
        />
        <Button
          block
          className="btn-morphing"
          color="info"
          size="lg"
          onClick={sendComment}
            >
            Опубликовать
        </Button>
      </>
    )
  };

  const disableComment = () => <p style={{fontSize: '20pt'}}>{text}</p>;

  return (
    <Container>
      <Row className='justify-content-md-center align-items-center'>
        <Col md={2} className="justify-content-md-center align-items-center">
        <h4 className="text-center">{name}</h4>
          <div className="photo-container" >
            <img
              alt="..."
            src={srcImage}
          />
          </div>
        </Col>
        <Col>
          {!disable ? activeComment() : disableComment() }
        </Col>
      </Row>

    </Container>
  )
}

export default Comment;
