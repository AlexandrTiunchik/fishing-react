import React, { useContext } from "react";
import { Link } from "react-router-dom";
import * as ROUTES from '../../constants/routeConstants'
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row
} from "reactstrap";
import AuthService from "../../services/auth";
import {StateContext} from "../../index";

// core components

function SignUp(props) {

  const [ user, setUser ] = React.useState({
    loginName: '',
    password: '',
    avatar: ''
  });
  const [firstFocus, setFirstFocus] = React.useState(false);
  const [lastFocus, setLastFocus] = React.useState(false);
  const [emailFocus, setEmailFocus] = React.useState(false);
  const [userState, dispatchState] = useContext(StateContext);

  const changeHandler = event => {
    const {value, name} = event.target;
    setUser({
      ...user,
      [name]: value
    });
  };

  const registration = async () => {
    try {
      const response = await AuthService().registration(user);
      dispatchState({type: 'SET_USER', payload: response});
      props.history.push(ROUTES.NEWS);
    } catch (error) {
      console.error(error.response)
    }
  };

  return (
    <>
      <div
        className="section section-signup"
        style={{
          backgroundImage: "url(" + require("assets/img/bg11.jpg") + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center",
          minHeight: "700px"
        }}
      >
        <Container>
          <Row>
            <Card className="card-signup" data-background-color="black">
              <Form action="" className="form" method="">
                <CardHeader className="text-center">
                  <CardTitle className="title-up" tag="h3">
                    Регистрация
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <InputGroup
                    className={
                      "no-border" + (firstFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons users_circle-08"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Ваше имя"
                      type="text"
                      name="loginName"
                      onChange={changeHandler}
                      onFocus={() => setFirstFocus(true)}
                      onBlur={() => setFirstFocus(false)}
                    />
                  </InputGroup>
                  <InputGroup
                    className={
                      "no-border" + (lastFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons text_caps-small"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Пароль"
                      type="password"
                      name="password"
                      onChange={changeHandler}
                      onFocus={() => setLastFocus(true)}
                      onBlur={() => setLastFocus(false)}
                    />
                  </InputGroup>
                  <InputGroup
                    className={
                      "no-border" + (emailFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons ui-1_email-85"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email..."
                      type="text"
                      name="email"
                      onChange={changeHandler}
                      onFocus={() => setEmailFocus(true)}
                      onBlur={() => setEmailFocus(false)}
                    />
                  </InputGroup>
                </CardBody>
                <CardFooter className="text-center">
                  <Button
                    className="btn-neutral btn-round"
                    color="dark"
                    onClick={registration}
                    outline
                    size="lg"
                  >
                    Регистрация
                  </Button>
                </CardFooter>
              </Form>
            </Card>
          </Row>
          <div className="col text-center">
            <Button
              className="btn-round btn-white"
              color="default"
              to={ROUTES.LOGIN}
              outline
              size="lg"
              tag={Link}
            >
              Войти
            </Button>
          </div>
        </Container>
      </div>
    </>
  );
}

export default SignUp;
