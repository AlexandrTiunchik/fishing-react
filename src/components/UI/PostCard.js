import React from "react";

// reactstrap components
import {
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardText,
} from "reactstrap";
import {Link} from "react-router-dom";

function PostCard({ srcImage, title, text, bait, link}) {
    return (
    <>
      <Card style={{ width: "20rem", margin: "20px 20px" }} >
        <CardImg
          alt="..."
          src={srcImage}
          top
        />
        <CardBody>
          <CardTitle tag="h4">{title}</CardTitle>
          <CardText>
            {text && text.substring(0, 100) + "..."}
          </CardText>
            <Link to={link}>Подробнее</Link>
        </CardBody>
        {bait}
      </Card>
    </>
  )
}

export default React.memo(PostCard);
