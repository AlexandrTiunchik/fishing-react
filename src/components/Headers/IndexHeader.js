/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container } from "reactstrap";
// core components

function IndexHeader() {
  let pageHeader = React.createRef();

  React.useEffect(() => {
    if (window.innerWidth > 991) {
      const updateScroll = () => {
        let windowScrollTop = window.pageYOffset / 3;
        pageHeader.current.style.transform =
          "translate3d(0," + windowScrollTop + "px,0)";
      };
      window.addEventListener("scroll", updateScroll);
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll);
      };
    }
  });

  return (
    <>
      <div className="page-header clear-filter">
        <div
          className="page-header-image"
          style={{
            backgroundImage: "url(https://s1.best.kpcdn.net/belarus/fishing/images-by/tild3530-6366-4531-a333-386162633463___23.jpg)"
          }}
          ref={pageHeader}
        />
        <Container>
          <div className="content-center brand">
            <h1>РЫБАЛКА В БЕЛАРУСИ</h1>
          </div>
          <h6 className="category category-absolute">
          </h6>
        </Container>
      </div>
    </>
  );
}

export default IndexHeader;
