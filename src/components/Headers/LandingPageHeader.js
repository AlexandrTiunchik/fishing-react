import React from "react";

// reactstrap components
import { Container } from "reactstrap";

// core components

function LandingPageHeader(props) {
  let pageHeader = React.createRef();
  const { title, backgroundImage } = props;
  return (
    <>
      <div className="page-header page-header-small">
        <div
          className="page-header-image"
          style={{
            backgroundImage: `url("${backgroundImage}")`
          }}
          ref={pageHeader}
        />
        <div className="content-center">
          <Container>
            <h1 className="title">{title}</h1>
          </Container>
        </div>
      </div>
    </>
  );
}

export default LandingPageHeader;
