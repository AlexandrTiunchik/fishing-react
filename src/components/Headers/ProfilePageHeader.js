import React from "react";
import { Container, Row } from "reactstrap";

function ProfilePageHeader(props) {
  let pageHeader = React.createRef();

  React.useEffect(() => {
    if (window.innerWidth > 991) {
      const updateScroll = () => {
        let windowScrollTop = window.pageYOffset / 3;
        pageHeader.current.style.transform =
          "translate3d(0," + windowScrollTop + "px,0)";
      };
      window.addEventListener("scroll", updateScroll);
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll);
      };
    }
  });
  const { children } = props;

  return (
    <>
      <div
        className="page-header clear-filter page-header-small"
        filter-color="blue"
      >
        <div
          className="page-header-image"
          style={{
            backgroundImage: "url(" + require("assets/img/bg11.jpg") + ")"
          }}
          ref={pageHeader}
         />
        <Container>
            <div className="photo-container">
              <img alt="..." src={props.avatar} />
            </div>
          <Row className="justify-content-md-center align-items-center">
              <h3 className="title">
                 {
                  props.name && props.surname && props.name.length > 0 && props.surname.length > 0
                    ? `${props.name} ${props.surname}`
                    : `${props.loginName}`
                }
              </h3>
              {children}
          </Row>
          {/*<div className="content">*/}
          {/*  <div className="social-description">*/}
          {/*    <h2>{user.commentCount}</h2>*/}
          {/*    <p>Коментариев</p>*/}
          {/*  </div>*/}
          {/*  <div className="social-description">*/}
          {/*    <h2>{user.rang}</h2>*/}
          {/*    <p>Ранг</p>*/}
          {/*  </div>*/}
          {/*  <div className="social-description">*/}
          {/*    <h2>{user.postCount}</h2>*/}
          {/*    <p>Публикаций</p>*/}
          {/*  </div>*/}
          {/*</div>*/}
        </Container>
      </div>
    </>
  );
}

export default ProfilePageHeader;
