import React from "react";
import { Link } from "react-router-dom";
import {
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  UncontrolledTooltip
} from "reactstrap";

import HeaderHOC from './NavHOC';
import {MAIN, NEWS, LIBRARY, MAP, DISCUSSION, FIND, VIDEO} from '../../constants/routeConstants';

function IndexNavbar(props) {
  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [collapseOpen, setCollapseOpen] = React.useState(false);
  React.useEffect(() => {
    setNavbarColor("");
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 0 ||
        document.body.scrollTop > 0
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 0 ||
        document.body.scrollTop < 0
      ) {
        setNavbarColor("navbar-transparent");
      }
    };
    window.addEventListener("scroll", updateNavbarColor);
    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };
  });

  return (
    <>
      {collapseOpen ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open");
            setCollapseOpen(false);
          }}
        />
      ) : null}
      <Navbar className={"fixed-top " + navbarColor} expand="lg" color="info">
        <Container>
          <div className="navbar-translate">
            <NavbarBrand
              id="navbar-brand"
            >
              <Link to={MAIN}><img src={require('assets/img/zrUnND-WNTM.png')} alt="" style={{width: "40px", height: "40px"}} /></Link>
            </NavbarBrand>
            <UncontrolledTooltip target="#navbar-brand">
              Познавай подводный мир вместе с нами
            </UncontrolledTooltip>
            <button
              className="navbar-toggler"
              onClick={() => {
                document.documentElement.classList.toggle("nav-open");
                setCollapseOpen(!collapseOpen);
              }}
              aria-expanded={collapseOpen}
              type="button"
            >
              <span className="navbar-toggler-bar top-bar"/>
              <span className="navbar-toggler-bar middle-bar"/>
              <span className="navbar-toggler-bar bottom-bar"/>
            </button>
          </div>
          <Collapse
            className="justify-content-end"
            isOpen={collapseOpen}
            navbar
          >
            <Nav navbar>

              <NavItem>
                <Link to={FIND}>
                  <NavLink>
                    <i className="now-ui-icons ui-1_zoom-bold"/>
                    <p>Поиск</p>
                  </NavLink>
                </Link>
              </NavItem>

              <NavItem>
                <Link to={MAP}>
                  <NavLink>
                    <i className="now-ui-icons business_globe"/>
                    <p>Карта</p>
                  </NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <Link to={NEWS}>
                  <NavLink>
                    <i className="now-ui-icons files_paper"/>
                    <p>Новости</p>
                  </NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <Link to={DISCUSSION}>
                  <NavLink>
                    <i className="now-ui-icons education_agenda-bookmark"/>
                    <p>Обсуждения</p>
                  </NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <Link to={LIBRARY}>
                  <NavLink>
                    <i className="now-ui-icons business_bulb-63"/>
                    <p>Каталог рыб</p>
                  </NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <Link to={VIDEO}>
                  <NavLink>
                    <i className="now-ui-icons business_bulb-63"/>
                    <p>Видео о рыбалке</p>
                  </NavLink>
                </Link>
              </NavItem>
              {props.navBar}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default HeaderHOC(IndexNavbar);
