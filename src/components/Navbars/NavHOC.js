import React, {useContext} from 'react';

import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    NavItem,
    NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";
import { ADMIN, LOGIN } from "../../constants/routeConstants";
import { StateContext } from "../../index";
import Auth from "../../services/auth";

const ProfileButton = (userId) => {
  return (
    <>
      <DropdownItem to={`/profile/${userId.userId._id}`} tag={Link}>
        Профиль
      </DropdownItem>
      <DropdownItem onClick={() => {
        new Auth().logout().then(() => {
          window.location.reload();
        });
      }}>
        Выход
      </DropdownItem>
    </>
  )
};

const ROLE = {
    user: (data) => (
        <UncontrolledDropdown nav>
            <DropdownToggle
                caret
                color="default"
                nav
                onClick={e => e.preventDefault()}
            >
            <i className="now-ui-icons users_single-02 mr-1"/>
            <p>{data.loginName}</p>
            </DropdownToggle>
            <DropdownMenu>
              <ProfileButton userId={data.profileId} />
            </DropdownMenu>
        </UncontrolledDropdown>
    ),
    admin: (data) => (
        <>
            <UncontrolledDropdown nav>
                <DropdownToggle
                    caret
                    color="default"
                    nav
                    onClick={e => e.preventDefault()}
                >
                    <i className="now-ui-icons users_single-02 mr-1"/>
                  <p>{data.profile.name}</p>
                </DropdownToggle>
              <DropdownMenu>
                <DropdownItem to={`${ADMIN}`} tag={Link}>
                  Администрирование
                </DropdownItem>
                <ProfileButton userId={data.profileId} />
              </DropdownMenu>
            </UncontrolledDropdown>
        </>
    ),
    guest: () => (
        <NavItem>
            <Link to={LOGIN}>
                <NavLink>
                    <i className="now-ui-icons files_paper"/>
                    <p>Войти</p>
                </NavLink>
            </Link>
        </NavItem>
    )
};

export default function HeaderHOC(Component) {
    return function HeaderContainer(props) {
      const [userState] = useContext(StateContext);
      const { currentRole } = props;

      const navBar = ROLE[currentRole](userState) || ROLE.guest;

      return <Component navBar={navBar} />;
    };
};
