import React from "react";
import ModalBody from "reactstrap/es/ModalBody";
import ModalFooter from "reactstrap/es/ModalFooter";
import Button from "reactstrap/es/Button";
import ModalHeader from "reactstrap/es/ModalHeader";

export default function (props) {
  return (
    <>
      <ModalHeader>
        {props.title}
      </ModalHeader>
      <ModalBody>
        {props.text}
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.delete(props.data._id)}>Ok</Button>
        <Button color="secondary" onClick={props.closeModal}>Cancel</Button>
      </ModalFooter>
    </>
  )
};
