import React, { useState } from "react";
import ModalBody from "reactstrap/es/ModalBody";
import ModalFooter from "reactstrap/es/ModalFooter";
import ModalHeader from "reactstrap/es/ModalHeader";
import { InputGroup, InputGroupAddon, Button, Input  } from "reactstrap";

export default function (props) {
  const [state, setState] = useState({
    ...props.data
  });

  const changeHandler = e => {
    const {value, name} = e.target;
    setState({
      ...state,
      [name]: value
    });
  };

  const addImage = () => {
    if (!state.image && state.image.length <= 0) {
      return;
    }
    setState({
      ...state,
      images: [
        ...state.images,
        state.image
      ]
    });
  };


  return (
    <>
      <ModalHeader toggle={props.closeModal}>{props.text}</ModalHeader>
      <ModalBody>
        <Input
          name="name"
          className="mb-2"
          placeholder="Имя"
          value={state.name}
          onChange={changeHandler}
        />

        <Input
          name="surname"
          className="mb-2"
          placeholder="Фамилия"
          value={state.surname}
          onChange={changeHandler}
        />

        <Input
          name="avatar"
          className="mb-2"
          placeholder="Аватар"
          value={state.avatar}
          onChange={changeHandler}
        />

        <div className="d-flex row justify-content-around">
          {state.images.map(image => (
            <img className="col-5 m-1" src={image} alt="..."/>
          ))}
        </div>

        <div>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button
                onClick={addImage}
                style={{margin: 0}}
              >
                Добавить картинку
              </Button>
            </InputGroupAddon>
            <Input
              name="image"
              onChange={changeHandler}
            />
          </InputGroup>
        </div>

        <Input
          style={{maxHeight: 300, height: 300}}
          name="about"
          className="mb-2"
          placeholder="Описание"
          type="textarea"
          value={state.about}
          onChange={changeHandler}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.submit(state, state._id)}>Принять</Button>
        <Button color="secondary" onClick={props.closeModal}>Закрыть</Button>
      </ModalFooter>
    </>
  )
}
