import React, {useState} from "react";
import Input from "reactstrap/es/Input";
import ModalBody from "reactstrap/es/ModalBody";
import ModalFooter from "reactstrap/es/ModalFooter";
import Button from "reactstrap/es/Button";

export default function (props) {
  const [state, setState] = useState({
    ...props.data
  });

  const changeHandler = e => {
    const {value, name} = e.target;
    setState({
      ...state,
      [name]: value
    });
  };

  return (
    <>
      <ModalBody>
        <Input
          name="title"
          className="mb-2"
          placeholder="Заголовок"
          value={state.title}
          onChange={changeHandler}
        />

        <Input
          name="titleImage"
          className="mb-2"
          placeholder="Ссылка на картинку"
          value={state.titleImage}
          onChange={changeHandler}
        />

        <Input
          name="text"
          className="mb-2"
          placeholder="Описание"
          type="textarea"
          value={state.text}
          onChange={changeHandler}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.submit(state, state._id)}>Принять</Button>
        <Button color="secondary" onClick={props.closeModal}>Закрыть</Button>
      </ModalFooter>
    </>
  )
}
