import Api from './api'
import axios from 'axios';

import { API } from '../constants/apiConstants';

export class Auth extends Api {
  login = async (userData) => {
    try {
      const token = await this.post(API.AUTHENTICATION_API.LOGIN, userData);
      return await this.setAuthUser(token);
    } catch (error) {
      throw error;
    }
  };

  setAuthUser = async (responseData) => {
    const { data } = responseData;
    localStorage.setItem('token', data);
    axios.defaults.headers.common['Authorization'] = `Bearer ${data}`;
    const user = await this.get(`${API.USER_API.GET}`);
    return user.data;
  };

  registration = async (userData) => {
    try {
      const token = await this.post(API.AUTHENTICATION_API.REGISTRATION, userData);
      return await this.setAuthUser(token);
    } catch (error) {
      throw error;
    }
  };

  logout = async () => {
    try {
      localStorage.removeItem('token');
      delete axios.defaults.headers.common['Authorization'];
    } catch (error) {
      throw error;
    }
  };

  initApp = async () => {
    try {
      const token = localStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      const user = await this.get(`${API.USER_API.GET}`);
      return user.data;
    } catch (error) {
      throw error;
    }
  };

  get role() {
    return localStorage.getItem('role');
  }
}

export default Auth;
