import axios from 'axios';

import { BASE_URL } from '../constants/apiConstants';

export default class Api {
  async post(url, data) {
    return axios.post(`${BASE_URL}${url}`, data);
  }

  async get(url, params){
    return axios.get(`${BASE_URL}${url}`, params);
  }

  async put(url, data){
    return axios.put(`${BASE_URL}${url}`, data);
  }

  async delete(url) {
    return axios.delete(`${BASE_URL}${url}`);
  }
}