import Api from './api'
import axios from 'axios';

export class Find extends Api {

  find = async (query) => {
    axios.defaults.headers.common['x-rapidapi-host'] = `google-search3.p.rapidapi.com`;
    axios.defaults.headers.common['x-rapidapi-key'] = `6ca2db3c23msh67d11bb02f136f1p1cc3f5jsnff671e6e39fe`;
    return await axios.get(`https://google-search3.p.rapidapi.com/api/v1/search?q=${query}`);
  };

}

export default new Find();
